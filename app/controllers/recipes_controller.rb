require 'tempfile'
require 'fileutils'
class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy, :toggle_favorito]
  after_action :recarga_manifest, only: [:create, :toggle_favorito]
  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.favoritos(params[:favoritos])
  end
  
  def galeria
	@recipes = Recipe.galeria(params[:ingrediente])
  end
  # GET /recipes/1
  # GET /recipes/1.json
  def show
		
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
  end

  # GET /recipes/1/edit
  def edit
  end
  def toggle_favorito
    @recipe.favorito = !@recipe.favorito
    @recipe.save
    respond_to do |format|  
      format.html { redirect_to :back}
      format.js
    end
  end
  # POST /recipes
  
  # POST /recipes.json
  def create
    @recipe = Recipe.new(recipe_params)
    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render action: 'show', status: :created, location: @recipe }
      else
        format.html { render action: 'new' }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to recipes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_params
      params.require(:recipe).permit(:imagen, :descripcion, :ingrediente_principal, :dificultad, :tiempo, :ingredientes, :nombre, :plato)
    end
	
	def recarga_manifest
      path = 'public/appoffline.appcache'
      temp_file = Tempfile.new('appoffline.appcache')
      begin
        temp_file.write("CACHE MANIFEST\n")
        temp_file.write("# v #{Time.now.to_i}\n")
        f = File.open(path, 'r')
        2.times {f.gets}
        f.each_line do |line|
          temp_file.puts line
        end
        temp_file.rewind
        FileUtils.mv(temp_file.path, path)
      ensure
      temp_file.close
      temp_file.unlink
      end
	end

    
end
