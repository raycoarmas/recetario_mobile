class Recipe < ActiveRecord::Base
	mount_uploader :imagen, ImagenUploader
	default_scope {order("nombre COLLATE NOCASE ASC")}
  
	scope :favoritos, ->(fav = nil) {fav === 'true' ? where(favorito: true) : all}
	scope :galeria, -> (ingrediente = nil) { where( "ingrediente_principal =?" , ingrediente)}
end
