json.array!(@recipes) do |recipe|
  json.extract! recipe, :id, :imagen, :descripcion, :ingrediente_principal, :dificultad, :tiempo, :ingredientes, :nombre, :plato
  json.url recipe_url(recipe, format: :json)
end
