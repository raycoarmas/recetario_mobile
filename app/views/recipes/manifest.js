window.addEventListener('load', function(e) {
   windows.applicationCache.addEventListener('updateready', function(e) {
      if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
      // Browser download a new app cache.
      // Swap it in and reload the page to get the new hotness.
      window.applicationCache.swapCache();
      window.location.reload();
      } else {
         //Manifest didn't changed. Nothing new to server.
      }
   }, false);
}, false);
