class AddFavoritoToRecipes < ActiveRecord::Migration
  def change
    add_column :recipes, :favorito, :boolean, default: false
  end
	def self.down
    change_column_default :recipes, :favorito, nil
  end
end
