class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :imagen
      t.text :descripcion
      t.string :ingrediente_principal
      t.string :dificultad
      t.string :tiempo
      t.text :ingredientes
      t.string :nombre
      t.string :plato

      t.timestamps
    end
  end
end
